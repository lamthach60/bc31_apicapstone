import { sanPhamConTroller } from "./controller/sanPhamController.js";
import { sanPhamService } from "./service/sanPhamService.js";
let productList = [];
let idProduct = null;
let renderTable = (listSanPham) => {
  let contentHTML = "";
  for (let i = 0; i < listSanPham.length; i++) {
    let sanPham = listSanPham[i];
    let contentTr = `
    <tr>
      <td>
        ${sanPham.id}
      </td>
      <td>
        ${sanPham.name}
      </td>
      <td>
        ${sanPham.price}
      </td>
      <td>
        <img src=${sanPham.img} height="150" width="150" />
      </td>
      <td>
        ${sanPham.desc}
      </td>
      <td>
      <button class="btn btn-danger" onclick="xoaSanPham(${sanPham.id})">Xóa</button>
      <button class="btn btn-danger" data-toggle="modal" data-target="#myModal" onclick="layThongTinSanPham(${sanPham.id})">Sửa</button>
    </td>
    </tr>`;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tbodyContent").innerHTML = contentHTML;
};
let renderDanhSachService = () => {
  sanPhamService
    .showDanhSachService()
    .then((result) => {
      console.log(result);
      productList = result.data;
      renderTable(productList);
    })
    .catch((err) => {});
};
renderDanhSachService();

let xoaSanPham = (id) => {
  sanPhamService
    .deleteService(id)
    .then((result) => {
      console.log(result);
      renderDanhSachService();
    })
    .catch((err) => {});
};
window.xoaSanPham = xoaSanPham;

let themSanPham = () => {
  let sanPham = sanPhamConTroller.layThongTinTuForm();
  sanPhamService
    .addService(sanPham)
    .then((result) => {
      console.log(result);
      renderDanhSachService();
    })
    .catch((err) => {});
};

window.themSanPham = themSanPham;

let layThongTinSanPham = (id) => {
  idProduct = id;
  sanPhamService
    .layThongTinChiTietSanPham(idProduct)
    .then((result) => {
      console.log(result);
      sanPhamConTroller.showThongTinLenForm(result.data);
    })
    .catch((err) => {});
};
window.layThongTinSanPham = layThongTinSanPham;

let capNhatSanPham = (params) => {
  let sanPham = sanPhamConTroller.layThongTinTuForm();
  let newSanPham = { ...sanPham, id: idProduct };
  sanPhamService
    .capNhatSanPham(newSanPham)
    .then((result) => {
      sanPhamConTroller.showThongTinLenForm({
        name: "",
        price: "",
        img: "",
        desc: "",
      });
      renderDanhSachService();
    })
    .catch((err) => {});
};

window.capNhatSanPham = capNhatSanPham;
