const BASE_URL = "https://62e5615c20afdf238d7bbefb.mockapi.io/Products";

export let sanPhamService = {
  layDanhSachSanPham: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  deleteService: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
  addService: (sanPham) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: sanPham,
    });
  },
  layThongTinChiTietSanPham: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },
  capNhatSanPham: (sanPham) => {
    return axios({
      url: `${BASE_URL}/${sanPham.id}`,
      method: "PUT",
      data: sanPham,
    });
  },
};
