import { SanPham } from "./model/sanPhamModel.js";
import { sanPhamService } from "./service/sanPhamService.js";

let productList = [];
let renderTable = (list) => {
  let contentHTML = "";
  for (let index = 0; index < list.length; index++) {
    let sanPham = list[index];
    let content = `
                <div class="col-md-4 py-4 item_first">
                  <div class=" card card-item" style="width: 18rem;">
                    <div class="about_img">
                      <img src=${
                        sanPham.img
                      } class="card-img-top py-2" alt="SamSung">
                     </div>
                    <div class="card-body">
                        <h5 class="card-title">${sanPham.name}</h5>
                        <p class="card-text">${
                          sanPham.desc.length < 20
                            ? sanPham.desc
                            : sanPham.desc.slice(0, 20) + "..."
                        }.</p>
                        <p class="">${sanPham.price}</p>
                        <a href="#" class="btn btn-dark">Buy</a>
                    </div>
                </div>
            </div>
    `;
    contentHTML = contentHTML + content;
  }
  document.getElementById("product_row").innerHTML = contentHTML;
};

let renderDanhSachSP = (productList) => {
  sanPhamService
    .layDanhSachSanPham()
    .then((res) => {
      console.log(res);
      productList = res.data.map((sanPham) => {
        let { id, name, img, desc, price } = sanPham;
        return new SanPham(id, name, img, desc, price);
      });
      renderTable(productList);
    })
    .catch((err) => {
      console.log(err);
    });
};

renderDanhSachSP();
