export class SanPham {
  constructor(_id, _name, _img, _desc, _price) {
    this.id = _id;
    this.name = _name;
    this.img = _img;
    this.desc = _desc;
    this.price = _price;
  }
}
