export let sanPhamConTroller = {
  layThongTinTuForm: (params) => {
    let name = document.getElementById("name").value;
    let price = document.getElementById("price").value;
    let img = document.getElementById("image").value;
    let desc = document.getElementById("desc").value;
    let sanPham = {
      name: name,
      price: price,
      img: img,
      desc: desc,
    };
    return sanPham;
  },
  showThongTinLenForm: (sanPham) => {
    document.getElementById("name").value = sanPham.name;
    document.getElementById("price").value = sanPham.price;
    document.getElementById("image").value = sanPham.img;
    document.getElementById("desc").value = sanPham.desc;
  },
};
